from flask import Flask,request
import test_app 

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Bob from Song'   

@app.route('/add')
def add():
    a = request.args.get('a', default = 1, type = int)
    b = request.args.get('b', default = 1, type = int)
    return str(test_app.test_add(a,b))
@app.route('/sub')
def sub():
    a = request.args.get('a', default = 1, type = int)
    b = request.args.get('b', default = 1, type = int)
    return str(test_app.test_sub(a,b))

app.run(host='0.0.0.0', port=8088)  